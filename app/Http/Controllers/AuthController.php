<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('halaman.daftar');
    }

    public function kirim(Request $request)
    {
        $name = $request['nama'];
        $address = $request['address'];

        return view('halaman.welcome', compact('name','address'));
    }

}
